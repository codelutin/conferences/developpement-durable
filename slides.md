---
theme: seriph
class: text-center
highlighter: shiki
lineNumbers: false
info: false
drawings:
  persist: false
transition: slide-left
css: unocss
title: Développement durable
titleTemplate: '%s - Code Lutin'
fonts:
  sans: Lato
  serif: Roboto Slab
  mono: Fira Code
themeConfig:
  primary: green
layout: image
image: /images/prez-1.jpg
---

<div class="mb-4 absolute top-50 left-50 right-50">
    <div class="text-4xl text-primary text-opacity-60" style="font-weight:700;color:#026B00" >
      DÉVELOPPEMENT DURABLE
    </div> 
    <span class="text-2xl text-primary-lighter text-opacity-80" style="font-weight:400;color: black" >
      Et si on développait enfin des logiciels bons pour les gens et la planète ?
    </span>
</div>

<!-- <div class="absolute bottom-10 left-10" style="color: black">
  <span class="font-700">
    Aurélie Lopes et Cécilia Bossard
  </span>
</div> -->

<div class="absolute bottom-5 right-10" style="color: black">
  <span class="font-700">
    JUG Summer Camp 2023
  </span>
</div>

<!--

-->

---
layout: image-right
image: /images/presentation.jpg
---

<div class="mb-4 absolute top-20" style="width:100%">
  <div class="slidev-layout">
    <div class="flex">
      <div class="flex flex-col">
        <h1 class="text-6xl">Aurélie Lopes</h1>
        <span>UX/UI designeuse</span>
        <span>@Code Lutin</span>
      </div>
      <figure class="flex justify-center w-1/2">
        <img
          src="/images/licorne.jpeg"
          class="bg-gray-400 rounded-full object-cover h-30 w-30 border border-8 border-secondary-400 z-10"
        />
      </figure>
    </div>
    <div class="flex" style="padding-top: 30px">
      <div class="flex flex-col">
        <h1 class="text-6xl">Cécilia Bossard</h1>
        <span>Développeuse</span>
        <span>@Shodo Nantes</span>
      </div>
      <figure class="flex justify-center w-1/2">
        <img
          src="/images/msInventor.jpg"
          class="bg-gray-400 rounded-full object-cover h-30 w-30 border border-8 border-secondary-400 z-10"
        />
      </figure>
    </div>
  </div>
</div>

---
layout: image-left
image: /images/joshua-lanzarini-FGvQKMP-iXY-unsplash.jpg
---

<div class="mb-4 absolute top-50" style="width:80%">
    <div class="text-xl text-primary text-hex-026B00" style="font-weight:700;" >
      "Le développement durable est un développement qui répond aux besoins du présent sans compromettre la capacité des générations futures de répondre aux leurs"
    </div>  
    <div><i>Rapport Brundtland, 1987</i></div>  
</div>

<!--
### Commençons par une définition
langage courrant, mais on perd le sens 

En 1987, une définition est donnée dans le rapport Brundtland, 

s'appuie sur 3 piliers : , social, environnemental et économique « People, Planet, Profit ») + un facteur indispensable => la gouvernance DONC participation de tous dans les décisions et leur mise en oeuvre

On a tous un rôle à jouer
-->

---
layout: image-right
image: /images/thisisengineering-raeng-sbVu5zitZt0-unsplash.jpg
---

<div class="mb-4 absolute top-50" style="width:80%">
    <div class="text-4xl text-primary text-hex-026B00" style="font-weight:700;" >
      En informatique c'est possible aussi
    </div>    
</div>

<!--
[Cécilia]  
- People => accessibilité et accès aux informations quel que soit le support utilisé
- Planet => éco-conception (Démarche qui vise à intégrer l'aspect environnemental dans toutes les étapes du cycle de vie d'un produit.)
- Profit => ne doit pas "plomber" le budget du projet
-->

---
layout: image-left
image: /images/deva-williamson-rHz-DkDgA1k-unsplash.jpg
---

<div class="mb-4 absolute top-50 " style="width:80%">
    <div class="text-3xl text-hex-026B00" style="font-weight:700;" >
      Préparons ensemble le meilleur gâteau du monde !
    </div> 
    <div class="text-xl text-hex-026B00" style="font-weight:700;" >
      (ça c'est ce qu'on espère réaliser)
    </div> 
</div>

<!--
### Ecologie, Informatique, Patisserie
C'est un peu bizarre comme transition me direz vous ! On a longtemps cherché comment présenter les choses le plus simplement possible, on a pensé que la bouffe c'était le mieux !!!

Imaginez qu'on est tous ensemble à la tête d'une pâtisserie.
On va réaliser ensemble le chef d’oeuvre, le gâteau signature, tous les clients vont se l'arracher, 

- design tendance qui plaira aux enfants et aux adultes, 
- avec le super robot patissier dernier cri, Cuisine moléculaire
- mais qui coutera pas un bras à produire 

Ohlala je suis impatiente, tout le monde fera Wahou j'en suis su
-->

---
layout: image-left
image: /images/dwsg8g-3720928f-d95a-4be3-8f20-026d3d677a16.jpg
---

<div class="mb-4 absolute top-50 " style="width:80%">
    <div class="text-3xl text-hex-026B00" style="font-weight:700;" >
      Le résultat n'est pas vraiment conforme à nos attentes
    </div> 
</div>

<!--
[Cécilia]  
A quoi ressemble votre création au final ? Tous les goûts y sont mélangés (chocolat, fruits, crème, …) au point qu’il est assez écoeurant.
La tenue du gâteau est assez particulière. On ne sait pas trop comment le manger (à la main ? avec des couverts ?)
Côté décor, vous avez voulu cibler les enfants, et il ressemble à un mélange Reine des neige, Spiderman, Pat’patrouille :-/
Et cerise sur le gâteau, impossible de définir la liste des allergènes présents.
Bref… Personne n’en achète. En plus d’encombrer les frigos, ce chef d’oeuvre fait baisser la réputation de la pâtisserie en chute libre.

[Aurélie]

Et pourquoi ? Que s’est-il passé ?
La recette n’a pas été très bien définie, on ne savait pas trop ce qui était attendu.
En plus, on ne savait pas que les gens pouvaient avoir des allergies ! Ça n'aide pas !
Et avec le budget défini, bah on a dû faire un peu avec les moyens du bord pour tenir, donc on a remplacé pas mal d’ingrédients par des alternatives moins chères.
Et avec le coût de l’énergie actuel, la durée de cuisson a été diminuée pour que ça ne plombe pas trop le budget.
-->

---
layout: image
image: images/gareth-hubbard-Z_fl2kybrm0-unsplash.jpg
---

<div class="mb-4 absolute top-50 left-50">
    <div class="text-4xl text-primary text-hex-026B00" style="font-weight:700;background-color:white;width:600px;height:150px;padding-top: 50px; text-align: center" >
      Reprenons depuis le début
    </div>    
    <div class="text-4xl text-primary text-hex-026B00" style="font-weight:700;background-color:white;width:600px;height:70px; text-align: center" >
      <carbon-favorite class="text-3xl text-red-400 mx-2"/>
      <carbon-earth class="text-3xl text-red-400 mx-2" />
      <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
    </div>
</div>

<!--
[Cécilia] 
Reprenons notre gâteau depuis le début en le regardant sous l'angle du développement durable. Donc People (les coeurs), Planet (les planètes) et Profit (les tirelires cochon).

Nous n'allons pas vous fournir de recette ni de formule magique, mais une liste de points de vigilance. Pas de panique, nous vous proposerons de télécharger une fiche pratique en fin de présentation !

Si vous mettez tout en place, c'est super génial ! Si vous n'en faites que 2 ou 3, c'st déjà super :) 

-->

---
layout: image
image: images/la-bonne-recette.png
---

<div class="mb-4 absolute top-50 left-50">
    <div class="text-4xl text-primary text-hex-026B00" style="font-weight:700;background-color:white;width:600px;height:150px;padding-top: 50px; text-align: center" >
      La bonne recette
    </div>    
</div>

<!--
#### On se pose 2 min pour trouver la bonne recette. 
C'est pas en mélangeant simplement des ingrédients avec une vague idée du résultat qu'on arrive à un chef d'oeuvre.

Pour un projet informatique c'est pareil, 
- 1/ l'UX, donc aux utilisateurs, 
- 2/ aux fonctionnalités qui répondent à un besoin et 
- 3/ comment on harmonise tout ça.
-->

---
layout: image-left
image: images/alex-haney-CAhjZmVk5H4-unsplash1sur2.png
---

<div class="mb-4 absolute top-50 " style="width:80%">
    <div class="text-3xl text-hex-026B00" style="font-weight:700;" >
      Qui va manger ce gâteau ?
    </div> 
    <div class="absolute top-20 left-15">
      <div>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
      </div> 
      <div>
        <carbon-earth class="text-3xl text-red-400 mx-2" />
        <carbon-earth class="text-3xl text-red-400 mx-2" />
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
      </div>
      <div>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
      </div>
    </div>
</div>

<!--
#### Premièrement définir les personas 
Persona : représentation synthéthique de nos catégories d'utilisateurs cible . Qui va manger notre gâteau ? pour qui je développe cette application ?

Etudier à qui l'on s'adresse permet de mieux les connaitre et de viser juste du premier coup ce qui est économiquement plus intéressant.

Pour notre gâteau si on s'adresse à des enfants, un gateau aux yaourts avec pleins de couleurs pour un effet arc en ciel va être l'idéal, tandis que pour un couple de jeunes mariés, la pièce montée mangue/ananas, insert Yuzu et glaçade à la fève de tonka sera forcément mieux mais beaucoup plus cher
-->

---
layout: image-right
image: images/alex-haney-CAhjZmVk5H4-unsplash2sur2.png
---

<div class="mb-4 absolute top-50">
      <div class="text-3xl text-hex-026B00" style="font-weight:700;" >
      Pourquoi on m’achète ce gateau?
    </div> 
     <div class="absolute top-25 left-15">
      <div>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
      </div> 
      <div>
        <carbon-earth class="text-3xl text-red-400 mx-2" />
        <carbon-earth class="text-3xl text-red-400 mx-2" />
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
      </div>
      <div>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
      </div>
    </div>
</div>

<!--
#### Ensuite définir les parcours utilisateurs

Pourquoi on me commande ce gateau ? dans quelle contexte va il être mangé ? 

Pour notre projet informatique 
- ? uniquement informations (site statique ou dynamique )? 
- base d'échanges (forum, actu, fraicheur des données,... )) 
- Appli métier très pointu ?  

Comprendre le contexte => interroger nos personas par rapport à cette situation. Comprendre ce qu'elles cherchent en venant sur ce site 

Pour notre gâteau nous n'allons pas proposer la même recette si c'est pour manger dans la rue rapidement entre 2 rdv, ou pour finir un bon repas entouré des gens qu'on aime
-->

---
layout: image
image: images/alex-haney-CAhjZmVk5H4-unsplash.jpg
---

<div class="mb-4 absolute top-50 left-35">
    <div class="text-4xl text-primary text-hex-026B00" style="font-weight:700;background-color:white;width:700px;height:150px;margin-top: 300px; text-align: center" >
      Revenir à une vue d'ensemble
    </div>    
</div>

<!--
#### Enfin, prendre du recul pour avoir une vue d'ensemble

hiérarchiser les fonctionnalités (de indispensable à très facultatif), 
et économiser le développement d'une fonctionnalité qui ferait surtout plaisir au développeur ou au client mais totalement inutile aux utilisateurs.

Parlez d'une démarche Mobile first ! 
- Plus le parcours utilisateur est défini et optimisé
- Plus ils seront content d’aller sur votre site
- Plus l’indice d’écoconception sera bon,
- Plus le profit économique sera bon également.
-->

---
layout: image-right
image: images/tangerine-newt-ogqaFRY7hAc-unsplash.jpg
---

<div class="mb-4 absolute top-50">
    <div class="text-3xl text-hex-026B00" style="font-weight:700;" >
      Choisir c'est renoncer... 
    </div> 
     <div class="text-3xl text-hex-026B00" style="font-weight:700;" >
      non c'est optimiser !
    </div> 
    <div class="absolute top-25 left-15">
      <div>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
      </div> 
      <div>
        <carbon-earth class="text-3xl text-red-400 mx-2" />
        <carbon-earth class="text-3xl text-red-400 mx-2" />
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
      </div>
      <div>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/>
      </div>
    </div>   
</div>

<!--
#### On connait leurs goûts, ce qu'ils veulent, attendent, les contraintes inhérentes. 

=> Avec toutes ces connaissances, on va pouvoir construction ce gâteau... ( mousses, de biscuits, ou avec un coeur coulant, ça ne donne pas la même impression lorsqu'on le mange.)

Principes ergonomiques qui répondent le mieux à notre situation. 
- Enchainement des pages, Chemins de navigations, Formats de tableau d'informations, etc.
- Guider l'utilisateur pour qu'il s'y retrouve.
=> envie des utilisateurs à revenir ou non sur notre site.
-->

---
layout: image-left
image: images/jacob-thomas-6jHpcBPw7i8-unsplash.jpg
---

<div class="mb-4 absolute top-50">
    <div class="text-3xl text-hex-026B00" style="font-weight:700;" >
      Prendre du recul
    </div>  
    <div class="absolute top-20 left-15">
      <div>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
      </div> 
      <div>
        <carbon-earth class="text-3xl text-red-400 mx-2" />
        <carbon-earth class="text-3xl text-red-400 mx-2" />
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
      </div>
      <div>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
      </div>
    </div>  
</div>

<!--
#### On n'oublie pas la cohérence de l'ensemble. 
relire, pour débusquer les incohérence. 

Pour notre interface c'est pareil, 
- l'enchainement des écrans a t'il été réfléchi et
- l'utilisateur va t'il s'y retrouver ? 
- les boutons d'actions sont ils au bon endroit et accessibles aux utilisateurs ? 

Avec notre vision sur l'ensemble de l'application, on en pense quoi ? Il est toujours temps à cette étape de réduire, modifier et lever les alertes pour les prochaines étapes, sans que ça surcoûte au projet.
-->

---
layout: image-right
image: images/calum-lewis-vA1L1jRTM70-unsplash.jpg
---

<div class="mb-4 absolute top-50">
    <div class="text-3xl text-hex-026B00" style="font-weight:700;" >
      Le secret c'est l'évolution
    </div> 
    <div class="absolute top-20 left-15">
      <div>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-gray-400 mx-2"/>
      </div> 
      <div>
        <carbon-earth class="text-3xl text-red-400 mx-2" />
        <carbon-earth class="text-3xl text-red-400 mx-2" />
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
      </div>
      <!-- <div>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/>
      </div> -->
    </div>   
</div>

<!--
#### Notre recette est bien ficelée mais on oublie pas l'après qui se prépare dès maintenant
Les utilisateurs évoluent, leurs besoins aussi, les fonctionnalités qui ont été retenues pour cette première version ne corresponderont peut être plus d'ici quelques temps

Noter les problématiques rencontrées, les évolutions possibles, interroger les utilisateurs sur leur envie à venir pour votre application. c'est aussi le gage de durabilité !

Et pour le process c'est pareil, peut être faudra t'il rencontrer plus d'utilisateurs la prochaine fois (ou moins).ou visiter plus de patisserie pour étudier la concurrence et gouter leur création !
-->

---
layout: image
image: images/passons-aux-preparatifs.png
---

<div class="mb-4 absolute top-50 left-50">
    <div class="text-4xl text-primary text-hex-026B00" style="font-weight:700;background-color:white;width:600px;height:150px;padding-top: 50px; text-align: center" >
      Passons aux préparatifs
    </div>    
</div>
<!--
On va détailler l'architecture technique
-->


---
layout: image-left
image: images/charlesdeluvio-GVxCDGLd4FI-unsplash.jpg
---

<div class="mb-4 absolute top-50 " style="width:80%">
    <div class="text-3xl text-hex-026B00" style="font-weight:700;" >
      Utiliser les bons outils
    </div>
    <div class="absolute top-20 left-15">
      <div>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
      </div> 
      <div>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-gray-400 mx-2"/>
        <carbon-earth class="text-3xl text-gray-400 mx-2"/>
      </div>
      <div>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/>
      </div>
    </div>
</div>

<!--
Ah ! Le choix des outils ! Toujours une décision délicate où chacun va vouloir mettre en avant son dernier gadget à la mode !
A-t'on besoin du dernier robot pâtissier à la mode qui fait tout (et même un peu trop) ou est-ce qu'on peut se contenter d'un fouet ?  
En développement, on va avoir la même problématique. Veut-on utiliser le dernier framework JS à la mode, même si on n'a pas franchement besoin de ce qu'il permet de faire ?  
Il faut vraiment vérifier l'utilité de certains outils et frameworks. Est-ce qu'ils n'ajoutent pas de la complexité non nécessaire dans l'arbre DOM ? Est-ce que le code HTML est bien conforme W3C ?

Toute cette complexité ajoutée et la non conformité des pages ajoute une charge supplémentaire sur les navigateurs des utilisateurs, car ce sont eux qui vont devoir corriger les problèmes "à la volée". Et sur une machine vieillissante, ça peut entraîner pas mal de lags.

-->

---
layout: image-left
image: images/pexels-soumith-soman-5985991.png
---

<div class="mb-4 absolute top-50" style="width:80%">
    <div class="text-3xl text-hex-026B00" style="font-weight:700;" >
      Réutilisation
    </div> 
    <div class="absolute top-20 left-15">
      <!-- <div>
        <carbon-favorite class="text-3xl text-hex-F5F5F5 mx-2"/>
        <carbon-favorite class="text-3xl text-hex-F5F5F5 mx-2"/>
        <carbon-favorite class="text-3xl text-hex-F5F5F5 mx-2"/>
      </div>  -->
      <div>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
      </div>
      <div>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
      </div>
    </div>
</div>

<!--
Est-ce qu'on va pouvoir utiliser des morceaux déjà faits ou préparés par ailleurs ? Est-ce qu'on ne va pas pouvoir profiter de la réalisation d'un gâteau similaire en parallèle pour réutiliser des choses ?   
C'est un peu le coeur de métier du développement : le copier/coller !  
Plus sérieusement, on utilise quasi quotidiennement des librairies ou des composants existants.  
Avant d'intégrer ces librairies telles qu'elles, il faut penser à supprimer toutes les fonctionnalités qu'elles embarquent et qu'on n'utilise pas, histoire d'alléger un peu la taille des livrables.
-->

---
layout: image-right
image: images/josephina-kolpachnikof-TK6g8A0M8DI-unsplash.jpg
---

<div class="mb-4 absolute top-50 " style="width:80%">
    <div class="text-3xl text-hex-026B00" style="font-weight:700;" >
      De la glace ou de la génoise ?
    </div> 
    <div class="absolute top-20 left-15">
      <!-- <div>
        <carbon-favorite class="text-3xl text-hex-F5F5F5 mx-2"/>
        <carbon-favorite class="text-3xl text-hex-F5F5F5 mx-2"/>
        <carbon-favorite class="text-3xl text-hex-F5F5F5 mx-2"/>
      </div>  -->
      <div>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-gray-400 mx-2"/>
      </div>
      <div>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/>
      </div>
    </div>
</div>

<!--
Est-ce que le gâteau contiendra de la glace et nécessitera une logistique lourde, ou est ce qu'il pourra rester  sur la table à température ambiante ?

De la même manière, est ce que les données devront rester fraîches et être mises à jour fréquemment ou non ?
Il faut bien définir quelle fraîcheur de données est attendue ? La ms, la s, l'heure, le jour, la semaine, ... ? Si vos utilisateurs veulent du temps réel, interrogez-les plus longuement; ce besoin est quand même assez rare
Plus la fraîcheur va être courte, plus les coûts engendrés vont être élevés.  
Il y aura beaucoup plus de requêtes réalisées pour actualiser les valeurs, ce qui va provoquer une charge plus importante pour le serveur et le client
-->

---
layout: image
image: images/quelle-decoration.png
---

<div class="mb-4 absolute top-50 left-50">
    <div class="text-4xl text-primary text-hex-026B00" style="font-weight:700;background-color:white;width:600px;height:150px;padding-top: 50px; text-align: center" >
      Pensons à la décoration
    </div>    
</div>

<!--
#### Oui pensons maintenant à la déco un gateau ça doit déjà être beau pour donner envie d'être mangé.

Comment faire les bons choix dans cette phase d'UI
-->

---
layout: image-right
image: images/graphe-tween-DYkiCphK3us-unsplash.jpg
---

<div class="mb-4 absolute top-50 " style="width:80%">
    <div class="text-3xl text-hex-026B00" style="font-weight:700;" >
      Transmettre le bon message
    </div> 
    <div class="absolute top-20 left-15">
      <div>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
      </div> 
      <div>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-gray-400 mx-2"/>
      </div>
      <div>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/> 
      </div>
    </div>
</div>

<!--
#### Choisir la charte graphique
Quelles couleurs choisir? quels thématique (minimaliste, excentrique, enfantin, ou autre) ?  Quelle émotion je souhaite transmettre avec ce gâteau ?
BUT : donner vie à votre site internet. Ne pas être un site lambda avec fonctionnalités sympa mais sous une forme générique.

Une fois que je sais (à peu près) où je veux aller, important => accessibilité. 
Dès le début car changement coute le moins cher et plus facile de faire marche arrière. 

Est ce que les contraste sont bons ? le choix de typos est lisible ? la hiérarchisation du texte est elle correcte ? utilisable par des lecteurs automatiques.... 

Penser à tous les humains sur cette planète, dans les différences et les particularités. on ne fait pas passer les messages importants par de la couleur uniquement sinon une personne daltonienne pourrait passer à côté de l'information.
-->

---
layout: image-left
image: images/thomas-william-fZTF8x8d0sg-unsplash.jpg
---

<div class="mb-4 absolute top-50 " style="width:80%">
    <div class="text-3xl text-hex-026B00" style="font-weight:700;" >
      Vouloir un gâteau tendance
    </div> 
    <div class="absolute top-20 left-15">
      <div>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
      </div> 
      <div>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
      </div>
      <div>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/> 
      </div>
    </div>
</div>

<!--
#### Tendance, Design, Moderne, qui claque, effet Wahou, 

On entend (et utilise) souvent ces termes quand on parle de graphisme et de style d'un objet. Mais comme le dit l'adage les goûts et les couleurs, ça ne se discute pas... enfin si au contraire même un peu trop parfois. Ce qui nous plait ne plait pas forcément à notre voisin et notre avis peut changer dans le temps. 

Ce n’est pas parce que c’est la mode qu’on doit forcément emballer notre gâteau dans une pate à sucre, ou le remplir de fleurs par exemple.

On rencontre des tendances de webdesign qui ne respectent pas toujours la diversité des utilisateurs 
- la surabondance de texte pour des dyslexique,
-  les vidéos à lancement automatiques pour les personnes ayant des troubles de la sphère autistique, 
- les pictos sans libellés laissé à seule appréciation de l'utilisateur quant à la fonction qui lui est associé...)
-->

---
layout: image-right
image: images/alexander-grey-L27M4LazH4g-unsplash.jpg
---

<div class="mb-4 absolute top-50 " style="width:80%">
    <div class="text-3xl text-hex-026B00" style="font-weight:700;" >
      Ajouter un peu de sucre ... pétillant ?
    </div> 
    <div class="absolute top-25 left-15">
      <div>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
      </div> 
      <div>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-gray-400 mx-2"/>
      </div>
      <div>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/> 
      </div>
    </div>
</div>

<!--
#### parfois les petits détails font la différence
On goûte le gateau et là on tombe sur du sucre pétillant qui explose dans la bouche. Nous voila retourné en enfance à jouer pendant quelques instants avec cette interaction nouvelle. L'expérience utilisateur est totale et on a le sourire pour le reste de la journée.

Sur un site web on peut penser aux micro-interactions, au bouton qui fait un halo quand on clique et qui me rassure sur le fait que j'ai bien cliqué. ça peut être aussi l'animation du menu qui s'ouvre en latéral et qui me fait comprendre que je reste dans la même application .

Mais il faut que ça reste des détails et ne pas rajouter un clignotement à chaque action sous peine de ressembler à un sapin de noel ou dans notre analogie culinaire, à un bonbon tête brulée ultra piquant !
-->

---
layout: image-left
image: images/pexels-eva-bronzini-5503338.jpg
---

<div class="mb-4 absolute top-50 " style="width:80%">
    <div class="text-3xl text-hex-026B00" style="font-weight:700;" >
      Et quand ce n'est plus tendance ?
    </div> 
    <div class="absolute top-25 left-15">
      <div>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
      </div> 
      <div>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
      </div>
      <!-- <div>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/>
      </div> -->
    </div>
</div>

<!--
#### Quelle durée de vie ? et comment valoriser notre gateau quand il ne se vendra plus ?

la société vit avec des modes qui passent et reviennent à un rythme qu'on ne connait pas à l'avance.
Penser à l'évolution graphique possible de votre site dès son lancement.

 Et simplifiez vous la vie en pensant correctement vos feuilles CSS ;)
-->

---
layout: image
image: images/Aux-fourneaux.png
---
<div class="mb-4 absolute top-50 left-50">
    <div class="text-4xl text-primary text-hex-026B00" style="font-weight:700;background-color:white;width:600px;height:150px;padding-top: 50px; text-align: center" >
      Aux fourneaux !
    </div>    
</div>

<!--
On passe au développement
-->

---
layout: image-right
image: images/pexels-cottonbro-studio-3992387.jpg
---

<div class="mb-4 absolute top-50 " style="width:80%">
    <div class="text-3xl text-hex-026B00" style="font-weight:700;" >
      Tester en continu
    </div> 
    <div class="absolute top-20 left-15">
      <div>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
      </div> 
      <div>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
      </div>
      <div>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/> 
      </div>
    </div>
</div>

<!--
Pour qu'un gâteau soit réussi, il faut surtout le goûter à chaque étape ;)

Et pour goûter un logiciel, on va le tester en continu, idéalement en utilisant des outils intégrés à la CI.
- Il faut s'assurer que le gâteau sera comestible (on testera donc l'accessibilité), 
- et que sa consistance ne nécessitera pas un temps de cuisson gigantesque ou de le repasser au four avant chaque bouchée (on testera donc l'éco-conception).  

Il existe des outils qui permettent de tester ces deux composantes (au moins en partie) automatiquement.
-->

---
layout: image-left
image: images/calum-lewis-rkT_TG5NKF8-unsplash.jpg
---

<div class="mb-4 absolute top-50 " style="width:80%">
    <div class="text-3xl text-hex-026B00" style="font-weight:700;" >
      Utiliser les bons ingrédients
    </div> 
    <div class="absolute top-20 left-15">
      <div>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
      </div> 
      <div>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-gray-400 mx-2"/> 
      </div>
      <div>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/> 
      </div>
    </div>
</div>

<!--
On va utiliser les ingrédients pour ce pour quoi ils sont faits ;) On ne va pas utiliser du sucre pour remplacer la farine.

Pour une application web, on va donc utiliser les balises HTML pour ce pour quoi elles sont faites !
Utiliser les balises link pour des liens, des boutons pour les actions, des balises "h" pour les titres etc pour vous assurez de la bonne interprétation par les outils/lecteurs automatiques
Bien structurer les balises pour que ce soit lisible et compréhensible (les navigations, les titres, le contenu, les sections, header, footer, etc)
-->

---
layout: image-right
image: images/pexels-ilyas-chabli-6533955.jpg
---

<div class="mb-4 absolute top-50 " style="width:80%">
    <div class="text-3xl text-hex-026B00" style="font-weight:700;" >
      Lister les ingrédients utilisés
    </div> 
    <div class="absolute top-20 left-15">
      <div>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
      </div> 
      <div>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-gray-400 mx-2"/>
        <carbon-earth class="text-3xl text-gray-400 mx-2"/> 
      </div>
      <div>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/> 
      </div>
    </div>
</div>

<!--
Dans tous gâteau, il est utile de préciser la liste des ingrédients utilisés pour éviter des réactions allergiques.

De la même manière, on va indiquer ce que contient notre page
- avec une structure claire et définie dans les balises HTML (une bonne hierarchisation des titres et des sections par exemple)
- avec des textes alternatifs sur les images et les contenus difficile à rendre par les lecteurs d'écran 
C'est bénéfique pour les lecteurs d'écran mais aussi pour les personnes avec une mauvaise connectivité
-->

---
layout: image-left
image: images/katrin-hauf-XyGfxCVbjCA-unsplash.jpg
---

<div class="mb-4 absolute top-50 " style="width:80%">
    <div class="text-3xl text-hex-026B00" style="font-weight:700;" >
      Utiliser les bonnes quantités
    </div> 
    <div class="absolute top-20 left-15">
      <div>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-gray-400 mx-2"/>
        <carbon-favorite class="text-3xl text-gray-400 mx-2"/> 
      </div> 
      <div>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
      </div>
      <div>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2 mx-2"/> 
      </div>
    </div>
</div>

<!--

Quand on suit une recette, on essaie de garder les proportions des ingrédients. On ne va pas mettre le double de farine préconisé juste parce qu'on a un paquet d'1kg et qu'on a la flemme de peser.

Alors pourquoi est ce qu'on le fait en informatique ?
- Optimisation de la taille des images.
On ne charge pas une image 4k pour un avatar par exemple
- Optimisation de la taille des réponses également : on ne charge pas toutes les informations personnelles d'un utilisateur pour n'afficher que son identifiant en haut de la page
- Lazy loading : on ne charge pas les ressources qui ne sont pas visibles 
-->

---
layout: image-right
image: images/sven-brandsma-3hEGHI4b4gg-unsplash.jpg
---

<div class="mb-4 absolute top-50 " style="width:80%">
    <div class="text-3xl text-hex-026B00" style="font-weight:700;" >
      Garder son plan de travail propre
    </div> 
    <div class="absolute top-25 left-15">
      <div>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-gray-400 mx-2"/> 
      </div> 
      <div>
        <!--<carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-gray-400 mx-2"/> -->
      </div>
      <div>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/> 
      </div>
    </div>
</div>

<!--
Quand on cuisine, on nettoie derrière soi pour faciliter la préparation des recettes suivantes.

On écrit du code lisible et réutilisable par les développeurs suivants.
Soyez gentils avec ceux qui passent après, ça pourrait être vous !

"Toujours laisser le code  dans un état meilleur que celui où vous l’avez trouvé" (Robert C. Martin).

Il peut être coûteux de mettre en place cette pratique, mais les gains sur le long terme sur les temps de développement vont ﬁnalement vite rentabiliser les coûts, surtout si vous vous occupez de la maintenance par la suite
-->

---
layout: image-left
image: images/taylor-grote-LqkFX2Km1a0-unsplash.jpg
---

<div class="mb-4 absolute top-50 " style="width:80%">
    <div class="text-3xl text-hex-026B00" style="font-weight:700;" >
      Préparer un gâteau facile à enfourner
    </div> 
    <div class="absolute top-25 left-15">
      <div>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-red-400 mx-2"/>
        <carbon-favorite class="text-3xl text-gray-400 mx-2"/> 
      </div> 
      <!-- <div>
        <carbon-earth class="text-3xl text-hex-F5F5F5 mx-2"/>
        <carbon-earth class="text-3xl text-hex-F5F5F5 mx-2"/>
        <carbon-earth class="text-3xl text-hex-F5F5F5 mx-2"/>
      </div> -->
      <div>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/> 
      </div>
    </div>
</div>

<!--
Il est plus facile de mettre dans le four un gâteau pas trop grand et qui ne tient pas en équilibre.

On va penser à faciliter la mise en prod et la maintenance pour les ops avec des applis simples à déployer.

La mise en production doit idéalement être une opération anodine et non douloureuse. On pense donc à la documentation et on automatise au maximum le process.
-->

---
layout: image
image: images/cuisson.png
---

<div class="mb-4 absolute top-50 left-50">
    <div class="text-4xl text-primary text-hex-026B00" style="font-weight:700;background-color:white;width:600px;height:150px;padding-top: 50px; text-align: center" >
      Maintenant au four
    </div>    
</div>

<!--
mettre en prod
-->


---
layout: image-right
image: images/leslie-saunders-1vhNkMq6_aM-unsplash.jpg
---

<div class="mb-4 absolute top-50 " style="width:80%">
    <div class="text-3xl text-hex-026B00" style="font-weight:700;" >
      Utiliser des fours mutualisés
    </div> 
    <div class="absolute top-20 left-15">
      <!-- <div>
        <carbon-favorite class="text-3xl text-hex-F5F5F5 mx-2"/>
        <carbon-favorite class="text-3xl text-hex-F5F5F5 mx-2"/>
        <carbon-favorite class="text-3xl text-hex-F5F5F5 mx-2"/>
      </div>  -->
      <div>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-gray-400 mx-2"/> 
      </div>
      <div>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/> 
      </div>
    </div>
</div>

<!--
Dans une pâtisserie, si on a plusieurs gâteaux à faire cuire, on les met tous ensemble dans un grand four.

En fait, c'est ce qu'on fait nous aussi avec les data center :)

La virtualisation => utiliser un même serveur physique pour plusieurs services.   
Optimisation des ressources physiques de la machine car utilisée par plusieurs applications.  
Economies possibles en terme d'investissement dans les machines physiques

 Et moins de nouvelles machines devront être produites, plus la pollution sera réduite (80% de la pollution du numérique provient de la phase de fabrication des serveurs et terminaux).
-->

---
layout: image-left
image: images/alex-lam-WOrdQ6Wgomw-unsplash.jpg
---

<div class="mb-4 absolute top-50 " style="width:80%">
    <div class="text-3xl text-hex-026B00" style="font-weight:700;" >
      Ne pas surcharger le four
    </div> 
    <div class="absolute top-20 left-15">
      <!-- <div>
        <carbon-favorite class="text-3xl text-hex-F5F5F5 mx-2"/>
        <carbon-favorite class="text-3xl text-hex-F5F5F5 mx-2"/>
        <carbon-favorite class="text-3xl text-hex-F5F5F5 mx-2"/>
      </div>  -->
      <div>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-gray-400 mx-2"/> 
      </div>
      <div>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/> 
      </div>
    </div>
</div>

<!--
Par contre, on essaie de ne pas faire s'effondrer le four sous le poids des gâteaux !

Pareil pour les serveurs, on ne les surcharge pas inutilement et on y installe que le minimum requis.

Les images sont souvent fournies avec de nombreux services inutiles dans votre cas d'usage. Pensez à les désinstaller, ça vous permettra de libérer des ressources mémoire et CPU.

Et si vous utilisez docker, soyez attentifs à la taille des images que vous utilisez. Il existe souvent des images plus légères qui n'embarquentqu'un seul service et pas un OS complet
-->

---
layout: image-right
image: images/pexels-alfo-medeiros-13388268.jpg
---

<div class="mb-4 absolute top-50 " style="width:80%">
    <div class="text-3xl text-hex-026B00" style="font-weight:700;" >
      Recycler les anciens fours
    </div> 
    <div class="absolute top-20 left-15">
      <!-- <div>
        <carbon-favorite class="text-3xl text-hex-F5F5F5 mx-2"/>
        <carbon-favorite class="text-3xl text-hex-F5F5F5 mx-2"/>
        <carbon-favorite class="text-3xl text-hex-F5F5F5 mx-2"/>
      </div>  -->
      <div>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-gray-400 mx-2"/> 
      </div>
      <div>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/> 
      </div>
    </div>
</div>

<!--
Et tant qu'on est à parler des fours mutualisés, on pense à les couper quand il n'y a plus de gâteau en train de cuire !

Et vos serveurs ? Est ce que vous avez toujours des serveurs de test ou de recette qui tournent alors qu'il n'y a plus d'évolutions sur l'application ?

Pensez à couper les serveurs inutilisés pour libérer les ressources. L'idéal est de prévoir un scénario de décommissionement des serveurs (à quelle date sera t-il coupé ?, ...)

Et quand un four n'est plus utilisé, il est débranché et revendu pour servir à d'autres artisans.  
Peut-être avez-vous des vieilles machines qui traînent et qui pourraient servir à des associations ?
-->

---
layout: image-right
image: images/andrew-valdivia-Way9MgCwFg8-unsplash.jpg
---

<div class="mb-4 absolute top-50 " style="width:80%">
    <div class="text-3xl text-hex-026B00" style="font-weight:700;" >
      Eviter les cookies
    </div> 
    <div class="absolute top-20 left-15">
      <!-- <div>
        <carbon-favorite class="text-3xl text-hex-F5F5F5 mx-2"/>
        <carbon-favorite class="text-3xl text-hex-F5F5F5 mx-2"/>
        <carbon-favorite class="text-3xl text-hex-F5F5F5 mx-2"/>
      </div>  -->
      <div>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
        <carbon-earth class="text-3xl text-red-400 mx-2"/>
      </div>
      <div>
        <carbon-piggy-bank class="text-3xl text-red-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/>
        <carbon-piggy-bank class="text-3xl text-gray-400 mx-2"/> 
      </div>
    </div>
</div>

<!--
Et oui ! On fait des gâteaux, pas des biscuits ;)

Les cookies sont transmis dans chaque requête HTTP, même pour les ressources statiques où ils ne servent à rien.
Héberger ses ressources dans un réseau de diﬀusion de contenu (CDN) sans cookie
permet d'alléger les requêtes de ces cookies.

Et idéalement, n'utilisez pas de cookies du tout !

Pour le suivi de l'analyse du traffic, il existe Matomo comme alternative à Google Analytics qui peut se configurer pour ne pas récolter de données personnelles, et ainsi éviter les affreux bandeaux de consentement aux cookies !  
Vous pouvez aussi vous passez d'outils d'analyse du traffic et vous contenter des logs serveurs. Votre analyse sera plus juste car certains utilisateurs bloquent les trackers

-->

---
layout: image
image: images/delaney-van-s_aGnOcfCq0-unsplash.jpg
---

<div class="mb-4 absolute top-50 left-50">
    <div class="text-4xl text-primary text-hex-026B00" style="font-weight:700;background-color:white;width:600px;height:150px;padding-top: 50px; text-align: center" >
      Tadaaaa !
    </div>    
</div>

<!--
#### Tadaaaaa! on a cherché on a pas trouvé mieux que les crêpes !
Les bretons ont vu justes depuis le début !

ça plait à tous les utilisateurs, ça se mange partout debout sur un coin de table à la fête foraire ou en gateau avec de la chantilly dans un resto chic. C'est économiquement viable, avec des ingrédient simples on sait vite les allergènes présents. 

Ce sera notre chef d'oeuvre ! vive les crêpes ;)
-->

---
layout: image-right
image: /images/edward-howell-zPVjna7YZ6Q-unsplash.jpg
---

<div class="mb-4 absolute top-50 left-50">
    <div class="text-4xl text-primary text-hex-026B00" style="font-weight:700;width:600px;height:150px;padding-top: 50px" >
      Pour résumer
    </div>    
</div>

<!--
#### Il n'y a pas de formule magique. 

Chaque action compte. On ne vous demande pas d'être parfaits dès demain, mais ayez en tête ces potentielles améliorations

- Moins consommer => Les ressources ne sont pas illimitées. L'informatique réduire sa consommation (d'énergie, de matériaux rare ou non, )
- Mieux partager => développez votre empathie avec vos utilisateurs. sans rajouter de l'inutileEt tout ça c'est bon pour le budget client !

Reprenons notre sens critique comme dans les années 2000
- la 5G n'est pas dispo partout !)
- Aller droit au but, fournir les informations utiles
- Abandonner la surabondance de ces dernières décénies
-->

---
layout: image
image: '/images/josh-calabrese-XXpbdU_31Sg-unsplash.jpg'
---
<div class="mb-4 absolute top-20 left-50">
    <div class="text-4xl text-primary text-hex-026B00" style="font-weight:700;width:600px;height:150px;padding-top: 50px" >
      Télécharger la liste des conseils
      <img style="padding-left: 125px; padding-top: 20px;" src="/images/qr-code.png">
      <p style="font-size:large; padding-left: 200px;">
      <a href="https://gitlab.nuiton.org/codelutin/conferences/developpement-durable/-/raw/main/developpement_durable.pdf?inline=false">Ou via ce lien</a>
      </p>
    </div>    
</div>

---
layout: image
image: '/images/josh-calabrese-XXpbdU_31Sg-unsplash.jpg'
---
<div class="mb-4 absolute top-50 left-50 right-50">
    <div class="text-4xl text-primary text-opacity-60" style="font-weight:700;color:#026B00" >
      Merci !
    </div> 
    <span class="text-2xl text-primary-lighter text-opacity-80" style="font-weight:400;color: black" >
      Avez-vous des questions ?
    </span>
</div>

---
layout: image
image: '/images/josh-calabrese-XXpbdU_31Sg-unsplash.jpg'
---
<div class="mb-4 absolute top-20 left-20 right-50">
    <div class="text-4xl text-primary text-opacity-60" style="font-weight:700;color:#026B00" >
      Références
    </div> 
    <span class="text-2xl text-primary-lighter text-opacity-80" style="font-weight:400;color: black" >
      <ul>
        <li>
          <a href="https://github.com/cnumr/best-practices">écoconception web : les 115 bonnes pratiques - Collectif Green IT</a>
        </li>
        <li>
          <a href="https://ecoresponsable.numerique.gouv.fr/publications/referentiel-general-ecoconception/">Référentiel général d'écoconception de services numériques (RGESN)</a>
        </li>
        <li>
          <a href="https://accessibilite.numerique.gouv.fr/">Référentiel général d'amélioration de l'accessibilité - RGAA Version 4.1</a>
        </li>
      </ul>
    </span>
</div>

---
layout: two-cols
background: '/images/josh-calabrese-XXpbdU_31Sg-unsplash.jpg'
---
<div class="mb-4 absolute top-5 left-10 right-20">
    <div class="text-4xl text-primary text-opacity-60" style="font-weight:700;color:#026B00" >
      Crédits
    </div> 
    <span class="text-xs" style="color: black">
      <ul>
        <li>Photo de <a href="https://unsplash.com/@sharonmccutcheon">Alexander Grey</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@alexhaney">Alex Haney</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@dokter_lam">Alex Lam</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@donovan_valdivia">Andrew Valdivia</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@calumlewis">Calum Lewis</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@charlesdeluvio">charlesdeluvio</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@delaneyvan">Delaney Van</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@biglaughkitchen">Deva Williamson</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@edwardhowellphotography">Edward Howell</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@fraenkly">Frank Holleman</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@gadgapho">Gareth Hubbard</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@graphetween">Graphe Tween</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@jacob225">Jacob Thomas</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@josephinakdesign">Josephina Kolpachnikof</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@joshcala">Josh Calabrese</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@jlanzarini">Joshua Lanzarini</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@trine">Katrin Hauf</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@saundelr">Leslie Saunders</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@marvelous">Marvin Meyer</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@moniqa">Monika Grabkowska</a> sur Unsplash</li>
      </ul>
    </span>
</div>
::right::
<div>
  <span class="text-xs"  style="color:black">
    <ul style="padding-top: 55px">
        <li>Photo de <a href="https://unsplash.com/@photos_by_lanty">Photos by Lanty</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@seffen99">Sven Brandsma</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@tangerinenewt">Tangerine Newt</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@taylor_grote">Taylor Grote</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@thisisengineering">ThisisEngineering RAEng</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@thomasw">Thomas William</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@heftiba">Toa Heftiba</a> sur Unsplash</li>
        <li>Photo de <a href="https://unsplash.com/@waparm">William</a> sur Unsplash</li>
        <li>Photo de <a href="https://www.pexels.com/fr-fr/@alfomedeiros">Alfo Medeiros</a> sur Pexels</li>
        <li>Photo de <a href="https://www.pexels.com/fr-fr/@cottonbro">cottonbro studio</a> sur Pexels</li>
        <li>Photo de <a href="https://www.pexels.com/fr-fr/@eva-bronzini">Eva Bronzini</a> sur Pexels</li>
        <li>Photo de <a href="https://www.pexels.com/fr-fr/@harrycunningham">Harry Cunningham</a> sur Pexels</li>
        <li>Photo de <a href="https://www.pexels.com/fr-fr/@ilyas">Ilyas Chabli</a> sur Pexels</li>
        <li>Photo de <a href="https://www.pexels.com/fr-fr/@soumithsoman">Soumith Soman</a> sur Pexels</li>
      </ul>
  </span>
</div>
