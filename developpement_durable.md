# Développement Durable

*Document écrit avec :heart: par Aurélie Lopes et Cécilia Bossard (Code Lutin)*

Cette liste de préconisations ne se veut pas exhaustive. Elle vous permettra d'avoir un premier aperçu des points de vigilance à avoir en tête lorsque l'on souhaite réaliser une application un minimum éco-conçue et accessible.

Légende des pictos : 

:heart: : bénéfique pour l'accessibilité

:earth_africa: : bénéfique pour l'éco-conception

:moneybag: : peu coûteux à mettre en place

## Phase UX

[Commencer par une définition large de l'UX _ méthode CUBIUX ](https://www.optimisation-conversion.com/webdesign/cubi-modele-conception-ux-business-optimiser-vos-conversions/)

1. **Définir les personas**

> Personnes :    :heart: :heart: :heart:  
> Planète :    :earth_africa: :earth_africa: :earth_africa:  
> Profit :    :moneybag: :moneybag: :moneybag:  

Qui va utiliser le site internet ? 
Pour qui je développe cette application ?

Enfants, Adultes ou jeunes retraités, plutôt citadins ou vivant à la campagne, père de famille débordé ou workingGirl débordante d'activités pro et perso... Etudier à qui l'on s'adresse permet de mieux les connaitre et de viser juste du premier coup ce qui est économiquement plus intéressant.

Il est possible d'avoir plusieurs profils cible pour un projet informatique

A lire :  
- [Exemple de construction de personas](https://www.usabilis.com/persona-ux-design/)
- [Critères ergonomiques Bastien et Scapin](https://www.usabilis.com/criteres-ergonomiques-bastien-et-scapin/)


2. **Définir les parcours utilisateurs**

> Personnes :    :heart: :heart: :heart:  
> Planète :    :earth_africa: :earth_africa: :earth_africa:  
> Profit :    :moneybag: :moneybag: :moneybag: 

- Pourquoi on me commande ce site ? 
- Dans quelle contexte il va être utilisé ?
- Quel besoin ? uniquement informations plus ou moins rafraichi (site statique ou dynamique )? 
- Est ce que ce sera une base d'échanges (forum, actu, => Quel besoin de fraicheur des données,... )
- Si c'est une appli métier, quel niveau de précision cela va demander ?  

Il faut interroger nos personas par rapport aux situations définies, pour comprendre ce qu'elles cherchent en venant sur ce site.

"La fonctionnalité la moins polluante est celle qui n'existe pas"

A lire :   
[Exemple de méthodologie pour faire des interview utilisateurs](https://www.manager-go.com/marketing/articles/comment-construire-votre-interview-utilisateur) 



3. **Revenir à une vue d'ensemble**

Changer son point de vue et avoir une vue d'ensemble

Définir la hiérarchie des fonctionnalités (de indispensable à très facultatif), et penser à la méthodologie Mobile First. Pas forcément pour les besoins de mobilité de vos utilisateurs mais pour la simplification (optimisation) de l'interface contrainte par l'exercice.

Plus le parcours utilisateur est défini et optimisé,  
Plus l’utilisateur est content d’aller sur le site/appli et s’y retrouve  
Plus l’indice d’écoconception est bon, et ne gaspille pas plus de ressources que nécessaire  
Plus le profit économique est bon.  

A lire :  
[Mobile First](https://www.journaldunet.com/ebusiness/internet-mobile/1154983-mobile-first-une-nouvelle-facon-de-concevoir-un-projet-digital/)

4. **Définition des principes ergonomiques adéquates**

> Personnes :    :heart: :heart: :heart:  
> Planète :    :earth_africa: :earth_africa: :earth_africa:  
> Profit :    :moneybag:  

Avec notre étude des utilisateurs on connait, leurs goûts, ce qu'ils veulent, attendent, les contraintes inhérentes, etc.

Il faut bien réfléchir aux principes ergonomiques qui répondent le mieux à notre situation. 
- Quel enchainement de page, 
- Les chemins de navigations, 
- Sous quels formats on affiche les tableaux d'informations, 
- Comment on guide l'utilisateur pour qu'il s'y retrouve dans le site,
- etc.

Nos choix auront des conséquences sur l'envie des utilisateurs à revenir ou non sur notre site.

Il est important de penser à l’impact que les choix ergonomiques ont sur les utilisateurs et la planète. 

Selon le choix pour lequel vous optez, les impacts sur nos 3 piliers pourront être différents :
- Penser à l'optimisation UX sans délaisser la SEO 
- Faut il mettre un scroll infini ou une pagination ? 

A lire :  
- [Ux et SEO référencement](https://www.kseo.biz/blog-optimisation-web/seo-ux-cro-3-leviers-pour-optimiser-son-site-web/)
- [Pagination ou scroll infini ? ](https://www.webrankinfo.com/astuces/pagination-scroll-infini)

5. **Cohérence de l'application**

> Personnes :    :heart: :heart: :heart:  
> Planète :    :earth_africa: :earth_africa: :earth_africa:  
> Profit :    :moneybag: :moneybag: :moneybag: 

On a une vision d'ensemble de notre site internet (avec des wireframes si possible)
- l'enchainement des écrans a t'il été réfléchi et est il identique partout ?
- l'utilisateur va t'il s'y retrouver ? la navigation est bonne ? on peut revenir en arrière ?
- les boutons d'actions sont ils au bon endroit, toujours au même endroit et accessibles aux utilisateurs ? 

Avec cette vision sur l'ensemble de l'application, on en pense quoi ? 

C'est à ce moment que les modifications coutent le moins au projet.

6. **Penser à l'après dès maintenant**

> Personnes :    :heart: :heart:  
> Planète :    :earth_africa: :earth_africa: :earth_africa:  

Les utilisateurs évoluent, leurs besoins aussi, les fonctionnalités qui ont été retenues pour cette première version ne corresponderont peut être plus d'ici quelques temps (mois, années ?). 

Et ce n'est pas parce qu'on a renoncé à certaines fonctionnalités qu'elles ne peuvent pas arriver dans une prochaine version.
Noter les problématiques rencontrées, les évolutions possibles, interroger les utilisateurs sur leur envie à venir pour votre application.C'est ça aussi le gage de durabilité de votre site internet !

Et pour le process c'est pareil, peut être faudra t'il rencontrer plus (ou moins) d'utilisateurs la prochaine fois. 

Références :  
- [écoconception web : les 115 bonnes pratiques - Collectif Green IT](https://github.com/cnumr/best-practices")
- [Référentiel général d'écoconception de services numériques (RGESN)](https://ecoresponsable.numerique.gouv.fr/publications/referentiel-general-ecoconception/)
- [Référentiel général d'amélioration de l'accessibilité - RGAA Version 4.1](https://accessibilite.numerique.gouv.fr/)


## Définition de l'architecture

1. **Créer une architecture modulaire**

> Planète :    :earth_africa: :earth_africa:   
> Profit :     :moneybag: :moneybag:   


Une architecture modulaire va permettre une plus grande flexibilité de l'application. Il sera possible d'y ajouter de nouvelles fonctionnalités plus facilement, ainsi que d'y intégrer de nouveaux modules au besoin.

L'application supportera plus facilement les montées en charge et les coûts de maintenance en seront également réduits.  
Le code sera plus efficient et consommera donc moins de ressources (et aura donc un moindre impact écologique).  

Même si les coûts seront plus élevés au départ, la maintenance et l'évolution de l'application coûteront moins cher sur le long terme.

Référence : ["Créer une architecture applicative modulaire"  (Référentiel d'éco-conception - bonne pratique 14)](https://github.com/cnumr/best-practices/blob/main/chapters/BP_014_fr.md)


2. **Utiliser les bons frameworks**

> Personnes :    :heart: :heart: :heart:  
> Planète :    :earth_africa:  
> Profit :    :moneybag: :moneybag:  

Vérifier l'utilité réelle de certains frameworks. N'ajoutent-ils pas de la complexité non nécessaire dans l'arbre DOM ?  Le code HTML généré est-il bien conforme ?

Certains framework (JS en particulier) génèrent beaucoup de "bruit" autour des composants HTML. Toutes ces balises ajoutées alourdissent l'arbre DOM de la page, et la rendent plus difficile à interpréter par les navigateurs.

De plus, il arrive que ce code généré ne soit pas conforme aux normes du W3C. En cas de code non conforme, c'est le navigateur qui corrigera dynamiquement les problèmes, ce qui induit une consommation de ressources sur les postes des utilisateurs et de potentiels ralentissement sur l'affichage de la page. 

De la même manière, une page non conforme sera plus difficilement interprétable par les lecteurs d'écran.

Ne pas utiliser de framework JS peut induire des coûts plus élevés (il va falloir développer soi-même des composants), mais l'équipe de développement aura un contrôle plus fin sur le fonctionnement de ces composants.

Référence : ["Valider les pages auprès du W3C" (Référentiel d'éco-conception - bonne pratique 31)](https://github.com/cnumr/best-practices/blob/main/chapters/BP_031_fr.md)


3. **Définir la fraîcheur des données attendue**

> Planète :    :earth_africa: :earth_africa: :earth_africa:   
> Profit :    :moneybag: :moneybag: :moneybag: 

Plus les données demandées devront être rafraîchies rapidement, plus la complexité et les coûts vont augmenter.

Il y aura beaucoup plus de requêtes réalisées pour actualiser les valeurs, ce qui va provoquer une charge plus importante pour le serveur et le client.

4. **Réutilisation**

> Planète :    :earth_africa: :earth_africa: :earth_africa:  
> Profit :    :moneybag: 

Pour éviter d'avoir à redévelopper des éléments, il peut être utile de faire appel à des librairies ou des composants existants.  
Ceci permet de réduire drastiquement les coûts de développement.

Attention néanmoins aux librairies qui sont utilisées. Certaines embarquent beaucoup de fonctionnalités qui ne sont pas utilisées dans votre contexte. 
Pensez à optimiser ces imports en ne conservant que les portions utilisées (cf [tree shaking](https://developer.mozilla.org/fr/docs/Glossary/Tree_shaking)).


Référence : ["N'utilisez que les portions indispensables des librairies JavaScript et frameworks CSS" (Référentiel d'éco-conception - bonne pratique 40)](https://github.com/cnumr/best-practices/blob/main/chapters/BP_040_fr.md)



## Charte graphique et maquettes UI

1. **Choix de la charte graphique**

> Personnes :    :heart: :heart: :heart:  
> Planète :    :earth_africa: :earth_africa:   
> Profit :    :moneybag:   

L'étude de nos futurs utilisateurs va nous être utile pour cette partie aussi. 

- Quelles couleurs choisir? 
- Quelle thématique (minimaliste, excentrique, enfantin, ou autre) ?  
- Quelle émotion je souhaite transmettre sur ce site ?

BUT : donner vie à votre site internet. Ne pas être un site lambda avec fonctionnalités sympa mais sous une forme générique.

Une fois que je sais (à peu près) où je veux aller, important => accessibilité. 
Dès le début car changement coute le moins cher et plus facile de faire marche arrière. 

- Est ce que les contraste sont bons ? 
- le choix de typos est lisible ? 
- la hiérarchisation du texte est elle correcte ? 
- utilisable par des lecteurs automatiques.... 

Penser à tous les humains sur cette planète, dans les différences et les particularités. on ne fait pas passer les messages importants par de la couleur uniquement sinon une personne daltonienne pourrait passer à côté de l'information. 

A lire :
- [Color Checker](https://coolors.co/contrast-checker/112a46-acc8e5)
- [Quelle police pour les dyslexique](https://blog.lexidys.com/2020/07/29/quelle-police-pour-dyslexiques/)
- [Hiérarchisation des informations](https://jujotte.fr/blog/la-hierarchie-de-linformation-dans-lui/)

2. **Faire attention aux travers des tendances actuelles**

> Personnes :    :heart: :heart: :heart:  
> Planète :    :earth_africa: :earth_africa: :earth_africa:  
> Profit :    :moneybag:   

Tendance, Design, Moderne, qui claque, effet Wahou, on entend (et utilise) souvent ces termes quand on parle de graphisme et de style d'un objet. Mais comme le dit l'adage "les goûts et les couleurs, ça ne se discute pas"... enfin si au contraire même un peu trop parfois. 
Ce qui nous plait ne plait pas forcément à notre voisin et notre avis peut changer dans le temps. 

On rencontre des tendances de webdesign qui ne respectent pas toujours la diversité des utilisateurs 
- la surabondance de texte pour des dyslexique,
- les vidéos à lancement automatiques pour les personnes ayant des troubles de la sphère autistique, 
- les pictos sans libellés laissé à seule appréciation de tous les utilisateurs quant à la fonction qui lui est associé...


3. **Penser aux animations et micro-interactions**

> Personnes :    :heart: :heart: :heart:    
> Planète :    :earth_africa:   
> Profit :    :moneybag:   

Sur un site web on peut penser aux micro-interactions, au bouton qui fait un halo quand on clique et qui me rassure sur le fait que j'ai bien cliqué. ça peut être aussi l'animation du menu qui s'ouvre en latéral et qui me fait comprendre que je reste dans la même application .

Mais il faut que ça reste des détails et ne pas rajouter un clignotement à chaque action sous peine de ressembler à un sapin de noel et à plomber notre éco index.

4. **Recyclabilité de l’application**

> Personnes :    :heart:   
> Planète :    :earth_africa: :earth_africa: :earth_africa:  

la société vit avec des modes qui passent et reviennent à un rythme qu'on ne connait pas à l'avance.
Penser à l'évolution graphique possible, mise à jour du thème graphique selon les tendances, … )

Selon la durée de vie projetée du site, et la cible d’utilisateurs => partir sur des tendances graphiques plus ou moins audacieuses.

les tendances sont elles optimisées => l’accessibilité pour tous et écoconçu ou gourmandes en ressources.
On peut vouloir les utiliser tout de suite ou attendre que celles ci soient optimisées dans un futur proche.

On pense aussi à l'optimisation des feuilles CSS. Plus le découpage du style sera fait de façon simple et intuitive, plus la maintenance et les modifications futures seront simples.

A lire :  
 [Exemple de tendances à ne pas adopter sans réfléchir](https://99designs.fr/blog/tendances/tendances-en-design-web/)
 [8 règles pour organiser son code CSS](https://alticreation.com/8-regles-organiser-code-css/)


## Développement

1. **Tester en continu**

> Personnes :    :heart: :heart: :heart:  
> Planète :    :earth_africa: :earth_africa: :earth_africa:   
> Profit :    :moneybag: :moneybag: :moneybag:  

Pour s'assurer du bon fonctionnement du logiciel, il est coutume de réaliser des tests qui sont exécutés régulièrement via l'intégration continue.  
Il est tout à fait possible d'y intégrer des outils qui vont réaliser des tests d'accessibilité ou d'éco-conception.

Ces tests automatisés ne pourront pas remplacer un audit manuel, mais permettrons d'avoir un aperçu de l'état dans lequel se trouve l'application et de vérifier si il y a eu des régressions sur ces indicateurs.

Il existe plusieurs outils pour tester l'accessibilité. En voici deux exemples qui s'intègrent bien dans une intégration continue :
- [Pa11y](https://pa11y.org/)
- [axe-core](https://github.com/dequelabs/axe-core)

Pour tester l'éco-conception d'une application, il existe un outil utilisable facilement dans le navigateur : [eco-index](https://github.com/cnumr/ecoindex-browser-plugin).  
Un CLI en python est également disponible, ce qui permet de l'utiliser dans des outils de tests automatiques ([https://pypi.org/project/ecoindex-cli/](https://pypi.org/project/ecoindex-cli/))

2. **Bien utiliser les balises HTML**

> Personnes :    :heart: :heart: :heart:  
> Planète :    :earth_africa: :earth_africa:  
> Profit :    :moneybag: 

HTML fournit un panel très large de balises. Il est assez rare de ne pas y trouver son bonheur !  
Pensez à utiliser les balises pour ce pour quoi elles sont faites (un bouton pour une action, un lien pour ... un lien, les balises titre avec des niveaux cohérents, etc). Ceci améliorera les performances du rendu du site mais surtout son accessibilité.

Et si le rendu graphique ne vous plaît pas, un peu de CSS devrait suffire à améliorer les choses :)


3. **Proposer des textes alternatifs aux images qui apportent de l'information**

> Personnes :    :heart: :heart: :heart:  
> Planète :    :earth_africa:   
> Profit :    :moneybag: 

Les balises `alt` permettent d'ajouter un texte alternatif aux images.
Ce texte est affiché à l'écran lors du chargement de l'image et sert aux lecteurs d'écran pour décrire le contenu de l'image.

L'ajout de ces descriptions n'est en général pas très complexe à mettre en place et améliore grandement l'utilisabilité du site pour les personnes ayant besoin d'utiliser un lecteur d'écran, mais aussi pour les personnes ayant une mauvaise connectivité (ils peuvent savoir par avance si ça vaut le coup pour eux d'attendre le chargement de l'image ou si ils peuvent l'ignorer)

4. **Optimiser la taille des images**

> Personnes :    :heart:  
> Planète :    :earth_africa: :earth_africa: :earth_africa:   
> Profit :    :moneybag:  

Les images sont souvent les éléments les plus lourds à charger dans une application web.  
L'optimisation de ces images va permettre d'accélerer les temps de chargement des pages et réduire la charge sur les équipements réseau.

Il existe des outils en ligne permettant de réduire la taille des images sans pour autant perdre en qualité.

Références :
- ["Ne pas redimensionner les images coté navigateur" (Référentiel d'éco-conception - bonne pratique 34)](https://github.com/cnumr/best-practices/blob/main/chapters/BP_034_fr.md)
- ["Optimiser les images vectorielles" (Référentiel d'éco-conception - bonne pratique 36)](https://github.com/cnumr/best-practices/blob/main/chapters/BP_036_fr.md)

 
5. **Garder son code propre et lisible**

> Personnes :    :heart: :heart:  
> Profit :    :moneybag: :moneybag:  

Qui ne s'est pas retrouvé à relire du code écrit dans l'urgence ? C'est rarement une expérience très agréable.

Pensez aux développeurs qui passeront sur votre code après vous. Est-il compréhensible ? Exprime t-il l'intention du métier ?

On pense également à appliquer la règle du boyscout "Toujours laisser un endroit dans un état meilleur que celui où vous l’avez trouvé" (Robert C. Martin).

Il peut être coûteux de mettre en place cette pratique, mais les gains sur le long terme sur les temps de développement vont finalement vite rentabiliser les coûts.

6. **Rendre le projet facilement exploitable par les ops**

> Personnes :    :heart: :heart:  
> Profit :    :moneybag: :moneybag:  

En général, ce ne sont pas les postes des développeurs qui partent en production. Il faut donc penser en amont aux contraintes des serveurs sur lesquels l'application va devoir tourner.

La mise en production doit idéalement être une opération anodine et non douloureuse. Pour cela, pensez à écrire une bonne documentation pour les ops qui vont s'en occuper et de rendre les actions les plus faciles et automatiques possibles.


## Gestion des serveurs

1. **Réduire le nombre de requêtes HTTP et la taille des résultats**

> Planète :    :earth_africa: :earth_africa: :earth_africa:  
> Profit :    :moneybag: :moneybag: 

Le temps de chargement d'une page est lié au nombre de requêtes HTTP que le navigateur va devoir exécuter pour récupérer les données.  
Moins il y aura de requêtes et moins le serveur sera sollicité et plus il pourra servir de pages à des utilisateurs différents. Et donc moins il y aura besoin d'investir dans de nouveaux serveurs.

Références :

- ["Quantifier précisément le besoin" (Référentiel d'éco-conception - bonne pratique 02)](https://github.com/cnumr/best-practices/blob/main/chapters/BP_002_fr.md)
- ["Limiter le nombre de requêtes HTTP" (Référentiel d'éco-conception - bonne pratique 09)](https://github.com/cnumr/best-practices/blob/main/chapters/BP_009_fr.md)
- ["Limiter le nombre d'appels aux API HTTP" (Référentiel d'éco-conception - bonne pratique v4 22)](https://github.com/cnumr/best-practices/blob/main/chapters/BP_4022_fr.md)


2. **Utiliser du lazy-loading**

> Personnes :    :heart: :heart:  
> Planète :    :earth_africa: :earth_africa: :earth_africa:  
> Profit :    :moneybag:  

Dans le cas de pages assez longues avec beaucoup de contenu, les utilisateurs ne consultent pas forcément la totalité de la page. Il n'est donc pas nécessaire de charger les ressources qui ne sont pas affichées.  
Utilisez pour ce faire le principe du lazy-loading qui n'ira récupérer les ressources qu'au moment de leur affichage.

Référence : ["Utiliser le chargement paresseux" (Référentiel d'éco-conception - bonne pratique 37)](https://github.com/cnumr/best-practices/blob/main/chapters/BP_037_fr.md)
 

3. **Installer le minimum requis sur les serveurs**

> Planète :    :earth_africa: :earth_africa:  
> Profit :    :moneybag:

Lorsqu'un nouveau serveur est installé, il n'est pas rare qu'il vienne par défaut avec tout un tas de fonctionnalités qui n'ont pas d'utilité dans le cadre de votre projet. Ces services sont consommateurs de ressources CPU et mémoire. 

Pensez à vérifier les services présents et désinstallez ceux qui ne sont pas utiles.

Si vous utilisez Docker pour vos déploiements, soyez attentifs à la taille des images que vous utilisez. Il existe souvent des alternatives plus légères.

Référence : ["Installer le minimum requis sur le serveur" (Référentiel d'éco-conception - bonne pratique 91)](https://github.com/cnumr/best-practices/blob/main/chapters/BP_091_fr.md)

 

4. **Utiliser des serveurs mutualisés**

> Planète :    :earth_africa: :earth_africa:  
> Profit :    :moneybag: :moneybag: 

La virtualisation a permis de pouvoir utiliser un même serveur physique pour plusieurs services. L'utilisation des ressources physiques de la machine est optimisée car utilisée par plusieurs applications.  
Il va donc y avoir des économies possibles en terme d'investissement dans les machines virtuelles. Et moins de nouvelles machines devront être produites, plus la pollution sera réduite (80% de la pollution du numérique provient de la phase de fabrication des serveurs et terminaux).


Référence : ["Utiliser des serveurs virtualisés" (Référentiel d'éco-conception - bonne pratique 89)](https://github.com/cnumr/best-practices/blob/main/chapters/BP_089_fr.md)


5. **Choisir un hébergeur "vert" et un fournisseur d'électricité écoresponsable**

> Planète :    :earth_africa: :earth_africa:   
> Profit :    :moneybag: :moneybag: :moneybag:

Le choix de l'hébergeur est une étape cruciale si on souhaite réduire son impact numérique.  
Il existe des hébergeurs considérés comme éco-responsables, mais ils restent plus onéreux que les autres.

Si vous souhaitez héberger vous-même votre application, pensez à regarder du côté des fournisseurs d'électricité éco-responsables.

Soyez néanmoins vigilants aux messages marketing pouvant s'approcher du green-washing.


Références :
- ["Choisir un hébergeur 'éco-responsable'" (Référentiel d'éco-conception - bonne pratique 86)](https://github.com/cnumr/best-practices/blob/main/chapters/BP_086_fr.md)
- ["Privilégier un fournisseur d'électricité écoresponsable" (Référentiel d'éco-conception - bonne pratique 87)](https://github.com/cnumr/best-practices/blob/main/chapters/BP_087_fr.md)



6. **Prévoir un plan de fin de vie des serveurs**

> Planète :    :earth_africa: :earth_africa:  
> Profit :    :moneybag: :moneybag: :moneybag:

Il faut anticiper la fin de vie de l'application, et penser à couper les serveurs qui ne sont plus utilisés (avez-vous encore des environnements de test ou de recette qui tournent toujours alors qu'il n'y a plus d'évolution sur l'application ?)  
L'idéal est de prévoir un scénario de décommissionement des serveurs (à quelle date sera t-il coupé ?, ...)


Référence : ["Mettre en place un plan de fin de vie du site" (Référentiel d'éco-conception - bonne pratique 85)](https://github.com/cnumr/best-practices/blob/main/chapters/BP_085_fr.md)



7. **Utiliser un CDN sans cookies**

> Personnes :    :heart: :heart:  
> Planète :    :earth_africa: :earth_africa: :earth_africa:   
> Profit :    :moneybag: :moneybag:  

Les cookies sont transférés dans chaque requête HTTP si le serveur les accepte. Or ils ne sont pas utiles pour les ressources. Héberger ses ressources dans un réseau de diffusion de contenu (CDN) sans cookie permet d'alléger les requêtes de ces cookies.

Et pour aller plus loin, pourquoi ne pas supprimer complètement les cookies ? ;-)  
[Matomo](https://fr.matomo.org/) permet par exemple de réaliser une analyse du traffic web sans nécessiter la présence de cookie de traçage.

Références :
- ["Utiliser un CDN" (Référentiel d'éco-conception - bonne pratique 98)](https://github.com/cnumr/best-practices/blob/main/chapters/BP_098_fr.md)
- ["Héberger les ressources (CSS/JS) sur un domaine sans cookie" (Référentiel d'éco-conception - bonne pratique 94)](https://github.com/cnumr/best-practices/blob/main/chapters/BP_094_fr.md)
- ["Optimiser la taille des cookies" (Référentiel d'éco-conception - bonne pratique 82)](https://github.com/cnumr/best-practices/blob/main/chapters/BP_082_fr.md)

